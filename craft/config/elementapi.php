<?php
namespace Craft;

return [
    'endpoints' => [
        'api/v1/projects.json' => [
            'elementType' => ElementType::Entry,
            'criteria' => ['section' => 'projects'],
            'transformer' => function(EntryModel $entry) {
               foreach ($entry->disciplines as $category) {
                  $disciplines[] = $category->title;
               }
                return [
                    'title' => $entry->title,
                    'url' => $entry->url,
                    'jsonUrl' => UrlHelper::getUrl("api/v1/work/{$entry->id}.json"),
                    'disciplines' => $disciplines,
                    'body' => (string) $entry->body

                ];
            },
        ],
        'api/v1/projects/<entryId:\d+>.json' => function($entryId) {
            return [
                'elementType' => ElementType::Entry,
                'criteria' => ['id' => $entryId],
                'first' => true,
                'transformer' => function(EntryModel $entry) {
                    return [
                        'title' => $entry->title,
                        'url' => $entry->url,
                        'body' => $entry->body,
                    ];
                },
            ];
        },
        'api/v1/careers.json' => function() {
            HeaderHelper::setHeader([
                'Access-Control-Allow-Origin' => '*'
            ]);

            return [
                'elementType' => 'Entry',
                'elementsPerPage' => 300,
                'criteria' => [
                    'section' => 'openPositions',
                ],
                'transformer' => function(EntryModel $entry) {
                    foreach ($entry->jobTags as $tag) {
                        $jobTags[] = $tag->title;
                    }
                    return [
                        'title' => $entry->title,
                        'id' => (int) $entry->id,
                        'slug' => $entry->slug,
                        'postDate' => $entry->postDate,
                        'jobTags' => $jobTags
                    ];
                },
            ];
        }
    ]
];