<?php

/**
 * For ANY route request
 * EXCEPT - /api 
 * Use index.twig
 */

return array(
   '(?!api\/).*' => 'index'
);
