let mix = require('laravel-mix');

let buildPath = 'build/'

mix.setPublicPath('public')
  .js('src/js/main.js', buildPath + 'js/')
  .sass('src/scss/app.scss', buildPath + 'css/')
  .browserSync({
    proxy: 'http://vue-craft.local/',
    files: [
      'craft/templates/**/*.twig'
    ]
  })