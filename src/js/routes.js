import Home from "./views/Home";
import Careers from "./views/Careers";
import Projects from "./views/Projects";
import Detail from "./views/Detail";
import New from "./views/New";

export const routes = [
  { path: "/", component: Home },
  { path: "/careers", component: Careers, name: "careers", props: true },
  { path: "/projects", component: Projects, name: "projects", props: true },
  { path: "/:slug/:id", component: Detail, name: "detail", props: true },
  { path: "/new", component: New, name: "new", props: true },
];
