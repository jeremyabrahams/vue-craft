import Vue from "vue";
import App from "./App";
import VueRouter from "vue-router";
import { routes } from "./routes";
import VueResource from "vue-resource";

Vue.use(VueRouter);
Vue.use(VueResource);

const router = new VueRouter({
  mode: "history",
  routes
});

new Vue({
  el: "#app",
  template: "<App/>",
  components: { App },
  router
});